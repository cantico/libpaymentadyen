<?php 

require_once dirname(__FILE__).'/../../programs/configuration.php';


function LibPaymentAdyen_getTestConfig()
{
    $config = new LibPaymentAdyen_Configuration();
    
    $config->name = 'Tests';
    $config->applicationName = 'Tests';
    $config->environement = \Adyen\Environment::TEST;
    $config->hmacKey = '';
    $config->notificationHmacKey = '';
    $config->merchantAccount = '';
    $config->skinCode = '';
    
    $config->username = '';
    $config->password = '';
    
    return $config;
}