<?php 

require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';


$GLOBALS['babDBHost'] = 'localhost';
$GLOBALS['babDBLogin'] = 'test';
$GLOBALS['babDBPasswd'] = '';
$GLOBALS['babDBName'] = 'test';

$babDB = $GLOBALS['babDB'] = new babDatabase();

//exec('mysql -u test -Nse "show tables" test | while read table; do mysql -u test -e "drop table $table" test; done');
exec('mysql -u test test < vendor/ovidentia/ovidentia/install/babinstall.sql');


$payment = bab_functionality::get('Payment');
/*@var $payment Func_Payment */


bab_functionality::get('LibOrm')->initMysql();
$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
$sql = $mysqlbackend->setToSql($payment->logSet());
$synchronize = new bab_synchronizeSql();
$synchronize->fromSqlString($sql);
