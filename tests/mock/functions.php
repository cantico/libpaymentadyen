<?php 


$babInstallPath = 'vendor/ovidentia/ovidentia/ovidentia/';
$GLOBALS['babInstallPath'] = $babInstallPath;

require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new bab_SessionMockStorage());

require_once $GLOBALS['babInstallPath'].'utilit/functionality.mock.class.php';



class bab_functionality extends bab_functionalityMock
{
    public static function includefile($path)
    {
        switch($path) {
            case 'Payment/Adyen':
                require_once dirname(__FILE__).'/../../programs/paymentadyen.func.php';
                return 'Func_Payment_Adyen';
    

            case 'Payment':
                require_once dirname(__FILE__).'/../../vendor/ovidentia/libpayment/programs/payment.func.php';
                return 'Func_Payment';
                
            case 'LibOrm':
                require_once dirname(__FILE__).'/../../vendor/ovidentia/liborm/programs/orm.class.php';
                return 'Func_LibOrm';
        }
    
        return parent::includefile($path);
    }
    
    
    /**
     * Returns the specified functionality object.
     *
     * If $singleton is set to true, the functionality object will be instanciated as
     * a singleton, i.e. there will be at most one instance of the functionality
     * at a given time.
     *
     * @param	string	$path		The functionality path.
     * @param	bool	$singleton	Whether the functionality should be instanciated as singleton (default true).
     * @return	bab_functionality	The functionality object or false on error.
     */
    public static function get($path, $singleton = true)
    {
        self::includeOriginalDirname($path);
        $classname = self::includefile($path);
    
        if (!$classname) {
            return false;
        }
    
        if ($singleton) {
            return bab_getInstance($classname);
        }
    
        return new $classname();
    }
}