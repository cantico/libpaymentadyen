## Passerelle de paiement Adyen

Cette fonctionnalité nécessite le module LibPayment.

### Page de paiement hébergée par Adyen

Les autres modules Ovidentia de paiement utilisent tous une page de paiement fournie par le prestataire fournisseur de la passerelle. Cela est possible avec Adyen en utilsant HPP "Hosted payment page"

https://docs.adyen.com/developers/hpp-manual

L'intégration de cette fonctionnalité n'a pas été réalisée sur ce module.



### API de paiement


#### Paramétrage d'encryption des données de CB

Le fichier javascript qui va réaliser l'encryption doit être chargé sur la page :


    $adyen = bab_functionality::get('Payment/Adyen');
    /*@var $adyen Func_Payment_Adyen */
    $page->addJavascriptFile($adyen->getCseUrl());

Les champs de formulaire a soumettre à la passerelle de paiement ne doivent pas contenir d'attribut `name`. Un attribut `data-encrypted-name` doit être utilisé à la place comme préconnisé dans la documentation :

https://docs.adyen.com/developers/easy-encryption#cardintegration

Exemple pour le champ "generationtime" :

    $W->Hidden()
        ->addAttribute('data-encrypted-name', 'generationtime')
        ->setValue(gmdate("Y-m-d\TH:i:s.000\Z"))

Après enregistrement du formulaire, les informations de carte bancaire sont cryptées et doivent être utilisées de la façon suivante :

    $cardPayment = $adyen->newCardPayment();
    $cardPayment->setEncyptedCard(bab_pp('adyen-encrypted-data'));
    

    

#### paiement récurent

l'intégration de ce module permet les d'effectuer des paiement récurents en utilisant une autorisation non capturée. Le montant de la première autorisation ne sera pas rélevé. Pour que cela fonctionne, il faut que la capture automatique soit désactivée dans le back office adyen. 

La capture automatique doit être activée dans le module LibPaymentAdyen pour que les paiements récurents effectués par la suite soit correctement effectés (dans le cas contraire, ils devrons être capturés à la main dans le back-office Adyen).

Créer un contrat de récurrence : `$adyen->authorizeRecurringPayment()`

Liste des contrats en fonction de l'identifiant du client : `$adyen->getRecurringContractDetails()`

Supprimer un contrat de récurrence : `$adyen->disableRecurringContractDetail()`


Pour effectuer un paiement avec un contrat de récurence, il faut utiliser la méthode `$adyen->doPayment()` avec comme paramètre un object `libpayment_RecurringPayment`.


#### Paiement simple


Il faut utiliser la méthode `$adyen->doPayment()` avec comme paramètre un object `libpayment_CardPayment`