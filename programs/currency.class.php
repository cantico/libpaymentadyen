<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
*/


/**
 * Amount conversion with API currency codes
 * @link https://docs.adyen.com/developers/api-manual#apicurrencycodes
 *
 */
class LibPaymentAdyen_Currency
{
    /**
     * Number of digits to remove from adyen amount value
     * @var array
     */
    private $decimals = array(
        libpayment_Payment::CURRENCY_EUR => 2,
        libpayment_Payment::CURRENCY_USD => 2,
        libpayment_Payment::CURRENCY_CFA => 2,
        libpayment_Payment::CURRENCY_CHF => 2,
        libpayment_Payment::CURRENCY_GBP => 2,
        libpayment_Payment::CURRENCY_CAD => 2,
        libpayment_Payment::CURRENCY_JPY => 0,
        libpayment_Payment::CURRENCY_MXP => 2,
        libpayment_Payment::CURRENCY_TRL => 2,
        libpayment_Payment::CURRENCY_AUD => 2,
        libpayment_Payment::CURRENCY_NZD => 2,
        libpayment_Payment::CURRENCY_NOK => 2,
        libpayment_Payment::CURRENCY_BRC => 2,
        libpayment_Payment::CURRENCY_ARP => 2,
        libpayment_Payment::CURRENCY_KHR => 2,
        libpayment_Payment::CURRENCY_TWD => 2,
        libpayment_Payment::CURRENCY_SEK => 2,
        libpayment_Payment::CURRENCY_DKK => 2,
        libpayment_Payment::CURRENCY_KRW => 2,
        libpayment_Payment::CURRENCY_SGD => 2
    );
    
    
    /**
     * @param string $currency
     * @throws Exception
     * 
     * @return int
     */
    protected function getFactor($currency)
    {
        if (!isset($this->decimals[$currency])) {
            throw new Exception('Unsupported currency');
        }
        
        return pow(10, $this->decimals[$currency]);
    }
    
    
    /**
     * Encode one amount to a adyen value
     * @param string $amount
     * @param string $currency
     * @return int
     */
    public function encode($amount, $currency)
    {
        $amount = (float) $amount;
        return (int) round($amount * $this->getFactor($currency));
    }
    
    /**
     * Encode one adyen value to amount in currency
     * @param int $value
     * @param string $currency
     * @return float
     */
    public function decode($value, $currency)
    {
        return round($value/$this->getFactor($currency), $this->decimals[$currency]);
    }
}