<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




class LibPaymentAdyen_Configuration {

	/**
	 * Name of this configuration.
	 * Not used by Adyen
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * Optional description of this configuration.
	 * Not used by Adyen
	 * 
	 * @var string 
	 * 
	 */
	public $description;
	
	
	/**
	 * Application name to set in API
	 * @var string
	 */
	public $applicationName;

    /**
     * Username to use in API
     * @var string
     */
	public $username;
	
	/**
	 * Password to use in API
	 * @var string
	 */
	public $password;
	
	
	/**
	 * Username to use in API for doPayout
	 * @var string
	 */
	public $storePayoutUsername;
	
	/**
	 * Password to use in API for doPayout
	 * @var string
	 */
	public $storePayoutPassword;
	
	
	/**
	 * Username to use in API for payout confirmation
	 * @var string
	 */
	public $reviewPayoutUsername;
	
	/**
	 * Password to use in API for payout confirmation
	 * @var string
	 */
	public $reviewPayoutPassword;
	
	
	/**
	 * Merchant acount
	 * @var string
	 */
	public $merchantAccount;
	
	/**
	 * Environement to use in API
	 * 
	 * \Adyen\Environment::TEST | \Adyen\Environment::LIVE
	 */
	public $environement;
	
	
	/**
	 * Skin code
	 * This is for Hosted Payment Pages (HPP)
	 * @see https://docs.adyen.com/developers/hpp-manual
	 * Create new skin in back office, in the skin menu
	 * @var string
	 */
	public $skinCode;
	
	/**
	 * HPP HMAC Key
	 * This is for Hosted Payment Pages (HPP)
	 * This can be the test key or the live key
	 * the the correct hmac according to the $environement property
	 */
	public $hmacKey;

	
	/**
	 * Verification of notifications fields
	 * @see https://docs.adyen.com/developers/api-manual#enablinghmaconnotifications
	 * @var string
	 */
	public $notificationHmacKey;

	
	
	/**
	 * URL to javascript file hosted by Adyen for Client Side Encryption
	 * @see https://docs.adyen.com/developers/easy-encryption#adyenhosted
	 * @var string
	 */
	public $cseUrl;
	
	
	/**
	 * Auto capture payment in the doPayment method
	 * @var bool
	 */
	public $autoCapture;
	
	/**
	 * Auto confirm payouts
	 * @var bool
	 */
	public $autoConfirm;

}




/**
 *
 * @return bab_registry
 */
function LibPaymentAdyen_getRegistry()
{
	$registry = bab_getRegistryInstance();

	$registry->changeDirectory('/LibPaymentAdyen');

	return $registry;
}



/**
 * Returns the names of all configurations.
 *
 * @return array
 */
function LibPaymentAdyen_getConfigurationNames()
{
	$registry = LibPaymentAdyen_getRegistry();

	$registry->changeDirectory('configurations');

	$names = array();

	while ($name = $registry->fetchChildDir()) {
		$name = substr($name, 0, -1);
		$names[$name] = $name;
	}

	return $names;
}





/**
 * Returns all configuration information in a LibPaymentAdyen_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return LibPaymentAdyen_Configuration
 */
function LibPaymentAdyen_getConfiguration($name = null)
{
	$registry = LibPaymentAdyen_getRegistry();

	if (!isset($name)) {
		$name = LibPaymentAdyen_getDefaultConfigurationName();
	}

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);

	$configuration = new LibPaymentAdyen_Configuration();
	$properties = 0;

	while ($key = $registry->fetchChildKey()) {
		$configuration->$key = $registry->getValue($key);
		$properties++;
	}
	
	if (!$properties) {
	    throw new Exception(sprintf(LibPaymentAdyen_translate('The selected configuration "%s" does not contain informations'), $name));
	}

	return $configuration;
}





/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function LibPaymentAdyen_setDefaultConfigurationName($name)
{
	$registry = LibPaymentAdyen_getRegistry();
	$identifier = $registry->setKeyValue('default', $name);
}





/**
 * Returns the default configuration name.
 *
 * @return string
 */
function LibPaymentAdyen_getDefaultConfigurationName()
{
	$registry = LibPaymentAdyen_getRegistry();
	return $registry->getValue('default');
}





/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function LibPaymentAdyen_renameConfiguration($originalName, $newName)
{
	$registry = LibPaymentAdyen_getRegistry();

	$registry->changeDirectory('configurations');

	return $registry->moveDirectory($originalName, $newName);
}


/**
 * Sets all configuration information.
 *
 * @param string name The name of the configuration to save.
 * @param LibPaymentAdyen_Configuration $configuration
 *
 * @return void
 */
function LibPaymentAdyen_saveConfiguration($name, LibPaymentAdyen_Configuration $configuration)
{
	$registry = LibPaymentAdyen_getRegistry();

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);
	
	foreach($configuration as $key => $value) {
	    
	    $registry->setKeyValue($key, $value);
	}

}





/**
 * Deletes the specified configuration.

 * @param string name The name of the configuration to delete.
 * @return bool
 */
function LibPaymentAdyen_deleteConfiguration($name)
{
	$registry = LibPaymentAdyen_getRegistry();
	$registry->changeDirectory('configurations');

	if (!$registry->isDirectory($name)) {
		return false;
	}

	$registry->changeDirectory($name);

	$registry->deleteDirectory();

	return true;
}
