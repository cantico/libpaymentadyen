<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';

require dirname(__FILE__).'/vendor/autoload.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();




/**
 *
 * @return Widget_Frame
 */
function LibPaymentAdyen_TpeEditor()
{
	$W = bab_Widgets();
	$editor = $W->Frame();


	$layout = $W->VBoxItems(

		$W->LabelledWidget(
			LibPaymentAdyen_translate('Name'),
			$W->LineEdit()
				->setMandatory(true, LibPaymentAdyen_translate('You must specify a unique name for this TPE.'))
				->addClass('widget-fullwidth')
				->setName('name')
		),
		$W->LabelledWidget(
			LibPaymentAdyen_translate('Description'),
			$W->TextEdit()
				->setLines(2)
				->addClass('widget-fullwidth')
				->setName('description')
		),
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Application name'),
	        $W->LineEdit()
	           ->addClass('widget-fullwidth'),
	        'applicationName',
	        LibPaymentAdyen_translate('Application name given to adyen api, default to babSiteName')
	    ),
	    
		$W->LabelledWidget(
			LibPaymentAdyen_translate('User name'),
			$W->LineEdit()
		      ->setMandatory(true, LibPaymentAdyen_translate('You must specify a user name.'))
		      ->addClass('widget-fullwidth'),
			'username'
		),
		$W->LabelledWidget(
			LibPaymentAdyen_translate('Password'),
			$W->LineEdit()
		      ->setMandatory(true, LibPaymentAdyen_translate('You must specify a password.'))
		      ->addClass('widget-fullwidth'),
			'password'
		),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Store payout user name'),
	        $W->LineEdit()
	        ->setMandatory(true, LibPaymentAdyen_translate('You must specify a user name.'))
	        ->addClass('widget-fullwidth'),
	        'storePayoutUsername'
        ),
        $W->LabelledWidget(
            LibPaymentAdyen_translate('Store payout password'),
            $W->LineEdit()
            ->setMandatory(true, LibPaymentAdyen_translate('You must specify a password.'))
            ->addClass('widget-fullwidth'),
            'storePayoutPassword'
        ),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Review payout user name'),
	        $W->LineEdit()
	        ->setMandatory(true, LibPaymentAdyen_translate('You must specify a user name.'))
	        ->addClass('widget-fullwidth'),
	        'reviewPayoutUsername'
	    ),
        $W->LabelledWidget(
            LibPaymentAdyen_translate('Review payout password'),
            $W->LineEdit()
            ->setMandatory(true, LibPaymentAdyen_translate('You must specify a password.'))
            ->addClass('widget-fullwidth'),
            'reviewPayoutPassword'
        ),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Merchant account'),
	        $W->LineEdit()
	        ->setMandatory(true, LibPaymentAdyen_translate('Merchant account'))
	        ->addClass('widget-fullwidth'),
	        'merchantAccount'
	    ),

		$W->LabelledWidget(
			LibPaymentAdyen_translate('Environement'),
			$W->Select()
		      ->addOption(\Adyen\Environment::TEST, LibPaymentAdyen_translate('Test'))
		      ->addOption(\Adyen\Environment::LIVE, LibPaymentAdyen_translate('Production')),
		    'environement'
		),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Skin code for Hosted Payment Pages'),
	        $W->LineEdit()
	           ->addClass('widget-fullwidth'),
	        'skinCode'
	    ),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('HMAC key for Hosted Payment Pages'),
	        $W->LineEdit()
	        ->addClass('widget-fullwidth'),
	        'hmacKey'
	    ),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('HMAC key for notifications signed fields'),
	        $W->LineEdit()
	        ->addClass('widget-fullwidth'),
	        'notificationHmacKey'
	    ),
	    
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Client Side Encryption javascript file hosted by Adyen'),
	        $W->UrlLineEdit()
	        ->addClass('widget-fullwidth'),
	        'cseUrl'
	    ),
	    
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Auto capture payments'),
	        $W->Checkbox(),
	        'autoCapture',
	        LibPaymentAdyen_translate('Check this setting if auto-capture is disabled on adyen back-office, recurring contract autorizations will not be captured')
	    ),
	    
	    $W->LabelledWidget(
	        LibPaymentAdyen_translate('Auto confirm payouts to third-party'),
	        $W->Checkbox(),
	        'autoConfirm'
	    )


	)->setVerticalSpacing(1, 'em');

	$editor->setLayout($layout);


	return $editor;
}



/**
 *
 * @param string $tpe
 */
function LibPaymentAdyen_editTpe($tpe = null)
{
	$W = bab_Widgets();

	$addon = bab_getAddonInfosInstance('LibPaymentAdyen');
	$addonUrl = $addon->getUrl();


	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));



	$form = $W->Form();
	$form->addClass('BabLoginMenuBackground');
	$form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
	$form->setName('tpe');

	$editor = LibPaymentAdyen_TpeEditor();

	$form->addItem($editor);

	$form->addItem(
		$W->FlowItems(
			$W->SubmitButton()
				->setLabel(LibPaymentAdyen_translate('Save configuration'))
				->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK),
			$W->Link(
				LibPaymentAdyen_translate('Cancel'),
				$addonUrl . 'systemconf&idx=displayTpeList'
			)->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL)
		)
		->setSpacing(1, 'em')
		->addClass(Func_Icons::ICON_LEFT_16)
	);

	$form->setHiddenValue('tg', bab_rp('tg'));
	$form->setHiddenValue('idx', 'saveTpe');

	if (isset($tpe)) {
		$title = LibPaymentAdyen_translate('Edit TPE configuration');

		$configuration = LibPaymentAdyen_getConfiguration($tpe);
		
		$tpe = array();
		foreach($configuration as $key => $value) {
		    $tpe[$key] = $value;
		}
		
		$form->setValues(
			array(
				'tpe' => $tpe
			)
		);
		$form->setHiddenValue('tpe[originalName]', $tpe['name']);
	} else {
		$title = LibPaymentAdyen_translate('New TPE configuration');
	}

	$page->setTitle($title);

	$page->addItem($form);

	$page->displayHtml();
}




function LibPaymentAdyen_saveTpe(Array $tpe)
{
	$configuration = new LibPaymentAdyen_Configuration();
	
	foreach($configuration as $key => $value) {
	    if (!property_exists($configuration, $key)) {
	        continue;
	    }
	    $configuration->$key = $tpe[$key];
	}

	if (isset($tpe['originalName']) && $tpe['name'] != $tpe['originalName']) {
		LibPaymentAdyen_renameConfiguration($tpe['originalName'], $tpe['name']);
	}

	LibPaymentAdyen_saveConfiguration($tpe['name'], $configuration);
}




function LibPaymentAdyen_displayTpeList()
{
	$W = bab_Widgets();

	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

	$page->addItem($W->Title(LibPaymentAdyen_translate('List of configured TPE')));

	$tpeNames = LibPaymentAdyen_getConfigurationNames();


	$tableView = $W->TableView();


	$addon = bab_getAddonInfosInstance('LibPaymentAdyen');
	$addonUrl = $addon->getUrl();

	$tableView->addItem(
		$W->Label(LibPaymentAdyen_translate('Name')),
		0, 0
	);
	$tableView->addItem(
		$W->Label(LibPaymentAdyen_translate('Username')),
		0, 1
	);

	$tableView->addItem(
		$W->Label(''),
		0, 2
	);

	$tableView->addColumnClass(4, 'widget-column-thin');

	$tableView->addSection('tpe');
	$tableView->setCurrentSection('tpe');


	$defaultName = LibPaymentAdyen_getDefaultConfigurationName();

	$row = 0;
	foreach ($tpeNames as $name) {

		$configuration = LibPaymentAdyen_getConfiguration($name);

		$nameLabel = $W->Label($configuration->name);
		if ($name == $defaultName) {
			$nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
		}

		$tableView->addItem(
			$W->VBoxItems(
				$nameLabel,
				$W->Label($configuration->description)->addClass('widget-small')
			)->addClass(Func_Icons::ICON_LEFT_16),
			$row, 0
		);
		$tableView->addItem(
			$W->Label($configuration->username),
			$row, 1
		);

		$tableView->addItem(
			$W->FlowItems(
				$W->Link(
					LibPaymentAdyen_translate('Edit'),
					$addonUrl . 'systemconf&idx=editTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),

				$W->Link(
					LibPaymentAdyen_translate('Set default'),
					$addonUrl . 'systemconf&idx=setDefaultTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),

				$W->Link(
						LibPaymentAdyen_translate('Delete'),
						$addonUrl . 'systemconf&idx=deleteTpe&tpe=' . $name
				)->setConfirmationMessage(sprintf(LibPaymentAdyen_translate('Are you sure you want to delete the TPE \'%s\'?'), $name))
				->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
			)
			->setSpacing(4, 'px')
			->addClass(Func_Icons::ICON_LEFT_16),
			$row, 2
		);

		$row++;
	}

	$page->addItem($tableView);

	$page->addItem(
		$W->FlowItems(
			$W->Link(
				LibPaymentAdyen_translate('Add configuration'),
				$addonUrl . 'systemconf&idx=editTpe'
			)->addClass('icon ' . Func_Icons::ACTIONS_LIST_ADD)
		)->addClass(Func_Icons::ICON_LEFT_16)
	);
	

	$page->displayHtml();
}





/* main */

if (!bab_isUserAdministrator())
{
	return;
}



$idx= bab_rp('idx', 'displayTpeList');

$addon = bab_getAddonInfosInstance('LibPaymentAdyen');


switch ($idx)
{
	case 'displayTpeList':
		$babBody->addItemMenu('list', LibPaymentAdyen_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayTpeList');
		LibPaymentAdyen_displayTpeList();
		break;

	case 'editTpe':
		$tpe = bab_rp('tpe', null);
		$editor = LibPaymentAdyen_editTpe($tpe);
		break;

	case 'saveTpe':
		$tpe = bab_rp('tpe', null);
		$editor = LibPaymentAdyen_saveTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'deleteTpe':
		$tpe = bab_rp('tpe', null);
		LibPaymentAdyen_deleteConfiguration($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'setDefaultTpe':
		$tpe = bab_rp('tpe', null);
		LibPaymentAdyen_setDefaultConfigurationName($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;
}
